@extends('layouts/main')
@section('content')


    @foreach($posts as $post)

        <div class="col-md-4">
            <h6>Kategorija: {{$post->cat}}</h6>
            <h2>{{str_limit($post->name,100)}}</h2>
            <p>{{str_limit($post->desc,100)}} </p>
            <p><a class="btn btn-default" href="post/{{$post->id}}" role="button">Rodyti visa informacija &raquo;</a></p>
        </div>
    @endforeach

    <div>
        {{$posts->links()}}
    </div>

@endsection